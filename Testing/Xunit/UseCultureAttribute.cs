﻿using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using Supernova.Foundation.Core;
using Xunit.Sdk;

namespace Supernova.Testing.Xunit
{
    /// <summary>
    /// An attribute to change the current thread culture and UI culture.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class UseCultureAttribute : BeforeAfterTestAttribute
    {
        private readonly Lazy<CultureInfo> _culture;
        private readonly Lazy<CultureInfo> _uiCulture;

        private CultureInfo _originalCulture;
        private CultureInfo _originalUICulture;

        /// <summary>
        /// Initializes a new instance of the <see cref="UseCultureAttribute"/> class.
        /// </summary>
        /// <param name="culture">The culture name to replace current thread culture and UI culture with.</param>
        public UseCultureAttribute(string culture) : this(culture, culture)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UseCultureAttribute"/> class.
        /// </summary>
        /// <param name="culture">The culture name to replace current thread culture with.</param>
        /// <param name="uiCulture">The culture name to replace current thread UI culture with.</param>
        public UseCultureAttribute(string culture, string uiCulture)
        {
            Guard.NotNull(culture, nameof(culture));
            Guard.NotNull(uiCulture, nameof(uiCulture));

            _culture = new Lazy<CultureInfo>(() => CultureInfo.GetCultureInfo(culture));
            _uiCulture = new Lazy<CultureInfo>(() => CultureInfo.GetCultureInfo(uiCulture));
        }

        /// <summary>
        /// Gets the culture.
        /// </summary>
        public CultureInfo Culture => _culture.Value;

        /// <summary>
        /// Gets the UI culture.
        /// </summary>
        public CultureInfo UICulture => _uiCulture.Value;

        /// <summary>
        /// This method is called before the test method is executed.
        /// Stores the current thread culture and UI culture and replaces them with the new cultures defined in the constructor.
        /// </summary>
        /// <param name="methodUnderTest">The method under test.</param>
        public override void Before(MethodInfo methodUnderTest)
        {
            base.Before(methodUnderTest);

            _originalCulture = Thread.CurrentThread.CurrentCulture;
            _originalUICulture = Thread.CurrentThread.CurrentUICulture;

            Thread.CurrentThread.CurrentCulture = Culture;
            Thread.CurrentThread.CurrentUICulture = UICulture;

            CultureInfo.CurrentCulture.ClearCachedData();
            CultureInfo.CurrentUICulture.ClearCachedData();
        }

        /// <summary>
        /// This method is called after the test method is executed.
        /// Restores the original thread culture and UI culture.
        /// </summary>
        /// <param name="methodUnderTest">The method under test.</param>
        public override void After(MethodInfo methodUnderTest)
        {
            base.After(methodUnderTest);

            Thread.CurrentThread.CurrentCulture = _originalCulture;
            Thread.CurrentThread.CurrentUICulture = _originalUICulture;

            CultureInfo.CurrentCulture.ClearCachedData();
            CultureInfo.CurrentUICulture.ClearCachedData();
        }
    }
}
