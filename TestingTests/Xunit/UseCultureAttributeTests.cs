﻿using System;
using System.Globalization;
using System.Threading;
using Supernova.Testing.Xunit;
using Xunit;

namespace Supernova.TestingTests.Xunit
{
    public class UseCultureAttributeTests
    {
        [Fact]
        public void UseCultureAttribute_NewInstance_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new UseCultureAttribute(null));
            Assert.Throws<ArgumentNullException>(() => new UseCultureAttribute(null, "en-US"));
            Assert.Throws<ArgumentNullException>(() => new UseCultureAttribute("en-US", null));
        }

        [Theory]
        [InlineData("en-US", "en-US")]
        [InlineData("en-US", "fr-FR")]
        [InlineData("fr-FR", "en-US")]
        [InlineData("da-DK", "de-DE")]
        public void UseCultureAttribute_NewInstance_WithCulture_SetsProperties(string culture, string uiCulture)
        {
            var attr1 = new UseCultureAttribute(culture);
            var attr2 = new UseCultureAttribute(culture, uiCulture);

            Assert.Equal(culture, attr1.Culture.Name);
            Assert.Equal(culture, attr1.UICulture.Name);
            Assert.Equal(culture, attr2.Culture.Name);
            Assert.Equal(uiCulture, attr2.UICulture.Name);
        }

        [Theory]
        [InlineData("en-US", "en-US")]
        [InlineData("en-US", "fr-FR")]
        [InlineData("fr-FR", "en-US")]
        [InlineData("da-DK", "de-DE")]
        public void UseCultureAttribute_ChangesCultureDuringTest(string culture, string uiCulture)
        {
            CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
            CultureInfo originalUICulture = Thread.CurrentThread.CurrentUICulture;

            var attr1 = new UseCultureAttribute(culture);
            var attr2 = new UseCultureAttribute(culture, uiCulture);

            attr1.Before(null);
            Assert.Equal(attr1.Culture, Thread.CurrentThread.CurrentCulture);
            Assert.Equal(attr1.UICulture, Thread.CurrentThread.CurrentUICulture);
            attr1.After(null);
            Assert.Equal(originalCulture, Thread.CurrentThread.CurrentCulture);
            Assert.Equal(originalUICulture, Thread.CurrentThread.CurrentUICulture);

            attr2.Before(null);
            Assert.Equal(attr2.Culture, Thread.CurrentThread.CurrentCulture);
            Assert.Equal(attr2.UICulture, Thread.CurrentThread.CurrentUICulture);
            attr2.After(null);
            Assert.Equal(originalCulture, Thread.CurrentThread.CurrentCulture);
            Assert.Equal(originalUICulture, Thread.CurrentThread.CurrentUICulture);
        }
    }
}
